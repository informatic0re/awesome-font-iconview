package com.example.android;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.homami.android.IconView;

public class MainActivity extends ListActivity {

    private String[] mAllIcons;
    private String[] mAllIconNames;
    private LayoutInflater mInflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        mAllIcons = getResources().getStringArray(R.array.all_icons);
        mAllIconNames = getResources().getStringArray(R.array.all_icon_names);
        getListView().setAdapter(new IconAdapter());

    }

    class IconAdapter extends BaseAdapter {

        private ViewHolder mHolder;

        @Override
        public int getCount() {
            return mAllIcons.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflateViews();
            } else {
                mHolder = (ViewHolder) convertView.getTag();
            }

            mHolder.mIcon.setIcon(mAllIcons[position]);
            mHolder.mName.setText(mAllIconNames[position]);

            return convertView;
        }

        private View inflateViews() {
            final View convertView = mInflater.inflate(R.layout.list_item, null);
            mHolder = new ViewHolder();
            mHolder.mIcon = (IconView) convertView.findViewById(R.id.icon);
            mHolder.mName = (TextView) convertView.findViewById(R.id.name);

            convertView.setTag(mHolder);
            return convertView;
        }

        public class ViewHolder {
            public IconView mIcon;
            public TextView mName;
        }
    }
}
